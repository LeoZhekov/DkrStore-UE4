# Dekaron-Store-UE4  
  
This is an Unreal Engine 4.20 project with assets from my [DkrStore-3D-scene](https://gitlab.com/LeoZhekov/DkrStore-3D-scene) Blender project.  
It is an interactive approach to the scene. [Demo 1 here](https://www.youtube.com/watch?v=t8DX7w0pkno) [Demo 2 here](https://www.youtube.com/watch?v=1yLUDidGiPQ)  
Functionality implemented:  
* Pick up of weapons
* Inspecting picked weapon
* Dropping a picked weapon
* Leaving it back on the same place

Running the project: 
> Clone and build VS project files (VS 2017+)  
> When playing the game, approach any weapon and press **E** to pick it up, **F** to drop it, **Right click holding down and moving the mouse** to rotate it. If you are within range of a placeholder (_shelf_) for the weapon, the **Drop** will be changed to **Leave** which leaves the weapon to the spot of the handle the crosshair is pointing to (hidden ingame but has the weapon's silhouette to indicate the position). 

Future improvements:  
  
* UI elements for *Pick up, Drop, Leave, Inspect* will be replaced with visuals for keys and an explanation will be given before the *exploration of the shop (scene)* is started.  

* Materials may differ. I have yet to learn how to implement better UE4 materials for wood, metal, floor, stone, emmision materials and the runes on the walls of the enchanter room.  

* Sounds of weapons clashing with metal/wood/stone whenever moving around. Footsteps perhaps?  

* Add weapon silhouettes to indicate their place.  <- Done

* Probably an .exe file compiled that can be run as a normal game for people to check out?