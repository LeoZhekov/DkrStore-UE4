// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

// ReSharper disable CppMemberFunctionMayBeConst
// ReSharper disable CppLocalVariableMayBeConst
// ReSharper disable CppParameterMayBeConst

#include "DkrStoreCharacter.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Grabber.h"
#include "HUDControlComponent.h"
#include "Shelf.h"
#include "DkrStore.h"
#include "DrawDebugHelpers.h"

//////////////////////////////////////////////////////////////////////////
// ADkrStoreCharacter

ADkrStoreCharacter::ADkrStoreCharacter() {
  // Set size for collision capsule
  GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
  // Create a CameraComponent	
  FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
  FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
  FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
}

void ADkrStoreCharacter::BeginPlay() {
  // Call the base class  
  Super::BeginPlay();
  Grabber = FindComponentByClass<UGrabber>();
  HUDControl = FindComponentByClass<UHUDControlComponent>();
  if (!Grabber || !HUDControl) {
    UE_LOG(LogTemp, Error, TEXT("Components missing from character BP"))
    return;
  }
  Grabber->SetReach(Reach);
}

//////////////////////////////////////////////////////////////////////////
// Input

void ADkrStoreCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
  // set up game play key bindings
  check(InputComponent);

  // Bind object events
  InputComponent->BindAction("Grab", IE_Pressed, this, &ADkrStoreCharacter::Grab);
  InputComponent->BindAction("Drop", IE_Pressed, this, &ADkrStoreCharacter::Drop);

  // Bind movement events
  InputComponent->BindAxis("MoveForward", this, &ADkrStoreCharacter::MoveForward);
  InputComponent->BindAxis("MoveRight", this, &ADkrStoreCharacter::MoveRight);
}

AActor* ADkrStoreCharacter::PickedActor() {
  if (Grabber->IsItemPicked()) {
    return Grabber->GetPickedActor();
  }
  return nullptr;
}

void ADkrStoreCharacter::SetItemIsBeingInspected(bool IsItemBeingInspected) {
  ItemIsBeingInspected = IsItemBeingInspected;
}

// All ray-tracing is done for setting up the HUD messages that appear. 
void ADkrStoreCharacter::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);

  if (CheckForItemPicked()) {
    if (CheckForShelf()) {
      HUDControl->ShelfInRange();
    } else {
      HUDControl->HasItem();
    }
  } else {
    if (CheckIfItemWithinRange()) {
      HUDControl->ItemInRange();
    } else {
      HUDControl->RemoveAll();
    }
  }
}

bool ADkrStoreCharacter::CheckForItemPicked() {
  return Grabber->IsItemPicked();
}

/* This here is linetraced twice with different channels in order to filter out the walls so that no item can be picked up from behind a wall..*/
bool ADkrStoreCharacter::CheckIfItemWithinRange() {
  FHitResult HitResult = LineTraceByChannel(ECC_PICKUP_CHANNEL);
  FHitResult WallResult = LineTraceByChannel(ECC_WALL_CHANNEL);
  auto ActorHit = HitResult.GetActor();
  auto WallHit = WallResult.GetActor();
  if (ActorHit && WallHit && ActorHit->GetName() == WallHit->GetName()) {
    return true;
  }
  return false;
}

bool ADkrStoreCharacter::CheckForShelf() {
  FHitResult HitResult = LineTraceByChannel(ECC_SHELF_CHANNEL);
  auto ActorHit = HitResult.GetActor();
  auto Shelf = Grabber->GetShelf();
  if (ActorHit && ActorHit->GetName() == Shelf->GetName()) {
    return true;
  }
  return false;
}

FHitResult ADkrStoreCharacter::LineTraceByChannel(ECollisionChannel Channel) {
  FVector StartLocation;
  FRotator Rotation;
  GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(StartLocation, Rotation);
  
  auto EndLocation = StartLocation + (Rotation.Vector() * Reach);
  FCollisionQueryParams QueryParams;
  FHitResult HitResult;

  switch (Channel) {
    case ECC_PICKUP_CHANNEL:
      GetWorld()->LineTraceSingleByObjectType(HitResult, StartLocation, EndLocation, FCollisionObjectQueryParams(Channel), QueryParams);
      break;
    case ECC_SHELF_CHANNEL:
      GetWorld()->LineTraceSingleByObjectType(HitResult, StartLocation, EndLocation, FCollisionObjectQueryParams(Channel), QueryParams);
      break;
    case ECC_WALL_CHANNEL:
      GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, Channel, QueryParams);
      break;
    default:
      break;
   }
  return HitResult;
}

void ADkrStoreCharacter::MoveForward(float Value) {
  if (Value != 0.0f) {
    // add movement in that direction
    AddMovementInput(GetActorForwardVector(), Value);
  }
}

void ADkrStoreCharacter::MoveRight(float Value) {
  if (Value != 0.0f) {
    // add movement in that direction
    AddMovementInput(GetActorRightVector(), Value);
  }
}

// ReSharper disable once CppMemberFunctionMayBeConst
void ADkrStoreCharacter::Grab() {
  if (!Grabber->IsItemPicked()) {
    Grabber->Grab();
  }
}

// ReSharper disable once CppMemberFunctionMayBeConst
void ADkrStoreCharacter::Drop() {
  if (Grabber->IsItemPicked()) {
    Grabber->Release();
  }
}
