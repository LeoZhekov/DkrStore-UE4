// Fill out your copyright notice in the Description page of Project Settings.
// ReSharper disable CppMemberFunctionMayBeConst

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Grabber.generated.h"

class AWeapon;
class AShelf;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DKRSTORE_API UGrabber final : public UActorComponent {
  GENERATED_BODY()

public:
  // Sets default values for this component's properties
  UGrabber();
  // Called every frame
  void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
  void Grab();
  void Release();
  bool IsItemPicked();
  void SetReach(float ReachToSet);

  /** Used in Character Blueprint to get the actor of the picked object. */
  AActor* GetPickedActor();
  AShelf* GetShelf() { return Shelf; }
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
  FVector DropOffset = FVector(0, 0, 0);
protected:
  // Called when the game starts
  void BeginPlay() override;

private:

  float Reach = 200;

  class UPhysicsHandleComponent* PhysicsHandle = nullptr;

  FHitResult GetObjectInReach();

  FVector GetReachLineStart();
  FVector GetReachLineEnd();
  FVector GetLineEndForInspect();
  bool ItemIsPicked = false;

  AShelf* Shelf = nullptr;
  AWeapon* Weapon = nullptr;
};
