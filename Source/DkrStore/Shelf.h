// Fill out your copyright notice in the Description page of Project Settings.
// ReSharper disable CppMemberFunctionMayBeConst

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shelf.generated.h"

UCLASS()
class DKRSTORE_API AShelf final : public AActor {
  GENERATED_BODY()

public:
  // Sets default values for this actor's properties
  AShelf();

protected:
  // Called when the game starts or when spawned
  void BeginPlay() override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
  class AWeapon* Weapon;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
  class AWeaponSilhouette* Silhouette;

public:
  AWeapon* GetWeapon() { return Weapon; }
  AWeaponSilhouette* GetSilhouette() { return Silhouette; }
};
