// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define ECC_PICKUP_CHANNEL ECC_GameTraceChannel1
#define ECC_SHELF_CHANNEL ECC_GameTraceChannel2
#define ECC_WALL_CHANNEL ECC_GameTraceChannel4