// Fill out your copyright notice in the Description page of Project Settings.

#include "Shelf.h"
#include "Weapon.h"
#include "WeaponSilhouette.h"
#include "Engine/GameEngine.h"

// Sets default values
AShelf::AShelf() {
  PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AShelf::BeginPlay() {
  Super::BeginPlay();
  if (!Weapon || !Silhouette) {
    if (GEngine) {
      const auto Message = FString::Printf(TEXT("%s has something missing"), *(GetName()));
      GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, Message);
    }
  }
}
