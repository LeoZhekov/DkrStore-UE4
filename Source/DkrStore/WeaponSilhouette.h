// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponSilhouette.generated.h"

UCLASS()
class DKRSTORE_API AWeaponSilhouette final : public AActor {
  GENERATED_BODY()

public:
  // Sets default values for this actor's properties
  AWeaponSilhouette();

protected:
  // Called when the game starts or when spawned
  void BeginPlay() override;

public:
  void SetActorHiddenTo(bool bIsHidden);
};
