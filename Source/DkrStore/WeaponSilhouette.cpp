// Fill out your copyright notice in the Description page of Project Settings.
// ReSharper disable CppLocalVariableMayBeConst
// ReSharper disable CppParameterMayBeConst

#include "WeaponSilhouette.h"

// Sets default values
AWeaponSilhouette::AWeaponSilhouette() {
  PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AWeaponSilhouette::BeginPlay() {
  Super::BeginPlay();
  SetActorHiddenTo(true);
}

void AWeaponSilhouette::SetActorHiddenTo(bool bIsHidden) {
  this->SetActorHiddenInGame(bIsHidden);
}
