// Fill out your copyright notice in the Description page of Project Settings.
// ReSharper disable CppMemberFunctionMayBeConst
// ReSharper disable CppLocalVariableMayBeConst

#include "HUDControlComponent.h"
#include "UMG/Public/Blueprint/UserWidget.h"

// Sets default values for this component's properties
UHUDControlComponent::UHUDControlComponent() {
  PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UHUDControlComponent::BeginPlay() {
  Super::BeginPlay();

  if (!ensure(PickUpKeyWidget) || !ensure(DropKeyWidget) || !ensure(InspectKeyWidget) || !ensure(LeaveOnShelfKeyWidget)
  ) { return; }

  auto PlayerController = GetWorld()->GetFirstPlayerController();

  PickUpWidget = CreateWidget(PlayerController, PickUpKeyWidget);
  DropWidget = CreateWidget(PlayerController, DropKeyWidget);
  InspectWidget = CreateWidget(PlayerController, InspectKeyWidget);
  LeaveOnShelfWidget = CreateWidget(PlayerController, LeaveOnShelfKeyWidget);
  AddAllToViewport();
  RemoveAll();
}

void UHUDControlComponent::RemoveAll() {
  Remove(PickUpWidget);
  Remove(DropWidget);
  Remove(InspectWidget);
  Remove(LeaveOnShelfWidget);
}

void UHUDControlComponent::HasItem() {
  RemoveAll();
  Add(DropWidget);
  Add(InspectWidget);
}

void UHUDControlComponent::ShelfInRange() {
  RemoveAll();
  Add(LeaveOnShelfWidget);
}

void UHUDControlComponent::ItemInRange() {
  RemoveAll();
  Add(PickUpWidget);
}

void UHUDControlComponent::AddAllToViewport() {
  PickUpWidget->AddToViewport();
  DropWidget->AddToViewport();
  InspectWidget->AddToViewport();
  LeaveOnShelfWidget->AddToViewport();
}

void UHUDControlComponent::Remove(UUserWidget* Widget) {
  if (Widget->Visibility != ESlateVisibility::Hidden) {
    Widget->SetVisibility(ESlateVisibility::Hidden);
  }
}

void UHUDControlComponent::Add(UUserWidget* Widget) {
  if (Widget->Visibility != ESlateVisibility::Visible) {
    Widget->SetVisibility(ESlateVisibility::Visible);
  }
}
