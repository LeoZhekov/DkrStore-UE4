// Fill out your copyright notice in the Description page of Project Settings.
// ReSharper disable CppMemberFunctionMayBeConst
// ReSharper disable CppLocalVariableMayBeConst
// ReSharper disable CppParameterMayBeConst

#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Weapon.h"
#include "WeaponSilhouette.h"
#include "Shelf.h"
#include "DkrStore.h"

// Sets default values for this component's properties
UGrabber::UGrabber() {
  PrimaryComponentTick.bCanEverTick = true;
}

void UGrabber::BeginPlay() {
  Super::BeginPlay();
  PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
  if (!PhysicsHandle) {
    UE_LOG(LogTemp, Error, TEXT("Physics handle missing"))
  }
}

void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  if (ItemIsPicked) {
    PhysicsHandle->SetTargetLocation(GetLineEndForInspect());
    auto TargetRotation = PhysicsHandle->GetGrabbedComponent()->GetOwner()->GetActorRotation();
    PhysicsHandle->SetTargetRotation(TargetRotation);
  }
}

void UGrabber::Grab() {
  auto HitResult = GetObjectInReach();
  auto ActorHit = HitResult.GetActor();
  auto Component = HitResult.GetComponent();
  if (ActorHit) {
    PhysicsHandle->GrabComponentAtLocationWithRotation(Component, NAME_None, Component->GetOwner()->GetActorLocation(), Component->GetOwner()->GetActorRotation());
    ItemIsPicked = true;
    Weapon = static_cast<AWeapon*>(Component->GetOwner());
    Shelf = Weapon->GetShelf();
    Shelf->GetSilhouette()->SetActorHiddenTo(false);
  }
}

void UGrabber::Release() {
  if (ItemIsPicked) {
    auto HitResult = GetObjectInReach();
    auto ActorHit = HitResult.GetActor();
    if (ActorHit) {
      auto ShelfHit = static_cast<AShelf*>(ActorHit);
      if (ShelfHit->GetName() == Shelf->GetName()) {
        auto ItemReleaseLocation = Weapon->GetStartupLocation() + DropOffset;
        auto ItemReleaseRotation = Weapon->GetStartupRotation();
        auto GrabbedComponent = PhysicsHandle->GetGrabbedComponent();
        GrabbedComponent->GetOwner()->SetActorLocation(ItemReleaseLocation);
        GrabbedComponent->GetOwner()->SetActorRotation(ItemReleaseRotation);
        PhysicsHandle->ReleaseComponent();
        ItemIsPicked = false;
        Shelf->GetSilhouette()->SetActorHiddenTo(true);
      }
    } else {
      PhysicsHandle->ReleaseComponent();
      ItemIsPicked = false;
    }
  }
}

bool UGrabber::IsItemPicked() {
  return ItemIsPicked;
}

void UGrabber::SetReach(float ReachToSet) {
  Reach = ReachToSet;
}

FHitResult UGrabber::GetObjectInReach() {
  auto QueryParams = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
  FHitResult HitResult;
  if (ItemIsPicked) {
    // Line-tracing for a shelf object - Collision preset "Shelf"
    GetWorld()->LineTraceSingleByObjectType(HitResult, GetReachLineStart(), GetReachLineEnd(), FCollisionObjectQueryParams(ECC_SHELF_CHANNEL), QueryParams);
  } else {
    //Line-tracing for a pickup object - Collision preset "Pick Up"
    GetWorld()->LineTraceSingleByObjectType(HitResult, GetReachLineStart(), GetReachLineEnd(), FCollisionObjectQueryParams(ECC_PICKUP_CHANNEL), QueryParams);
  }

  return HitResult;
}

FVector UGrabber::GetReachLineStart() {
  FVector Location;
  FRotator Rotation;
  GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(Location, Rotation);
  return Location;
}

FVector UGrabber::GetReachLineEnd() {
  FVector Location;
  FRotator Rotation;
  GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(Location, Rotation);
  return Location + (Rotation.Vector() * Reach);
}

AActor* UGrabber::GetPickedActor() {
  return PhysicsHandle->GetGrabbedComponent()->GetOwner();
}

FVector UGrabber::GetLineEndForInspect() {
  FVector Location;
  FRotator Rotation;
  GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(Location, Rotation);
  return Location + (Rotation.Vector() * (Reach * 0.7));
}
