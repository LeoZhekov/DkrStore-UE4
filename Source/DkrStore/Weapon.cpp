// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"
#include "Shelf.h"
#include "Engine/GameEngine.h"

// Sets default values
AWeapon::AWeapon() {
  PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay() {
  Super::BeginPlay();
  if (!Shelf) {
    if (GEngine) {
      const auto Message = FString::Printf(TEXT("%s is not attached to anything"), *(GetName()));
      GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, Message);
    }
    return;
  }

  StartupLocation = GetActorLocation();
  StartupRotation = GetActorRotation();
}
